---
title: Framadate
type: tool
featured-image: framadate.jpg
link: "https://poll.graz.social"
weight: 5
tags: ["Tool", "Organisieren", "Umfragen", "Abstimmungen"]
replaces: ["Doodle"]
label:
    name: framadate
    replacesTitle: true
---

Framadate ist ein Online-Dienst, der Euch bei der Absprache von Terminen oder der Entscheidungsfindung hilft. Es ist keine Registrierung erforderlich.

## Screenshot
![Framadate](../images/framadate.png)

## Features
- eine Umfrage erstellen
- Termine oder Entscheidungsoptionen zur Auswahl stellen
- einen Link zur Umfrage an Ihre Freunde oder Kollegen schicken
- Entscheidungen treffen


<span class="post-meta">Bild: 	&nbsp;[Velerio Bozzolan](https://commons.wikimedia.org/wiki/User:Valerio_Bozzolan)</span>
