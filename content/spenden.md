---
title: "Spenden"
showtoc: true
TocOpen: true
ShowBreadCrumbs: false
---

Eure Spenden machen es möglich. Wir sind eine Gemeinschaft, welche Infrastruktur für sich selbst bereitstellt. Kostenlos und ohne Gewinnabsicht. Wir behalten uns jedoch vor, Spenden ab dem [18. November 2022](https://graz.social/@wir/109365611369405242) auch als Aufwandsentschädigungen für die Moderator*innen und Admins zu verwenden. Etwaige Überschüsse werden als Reserven angelegt, oder können zum Ausbau der Dienste führen. Falls die Dienste eingestellt werden sollten, werden die Finanzreserven in möglichst gemeinschaftlicher Entscheidung an einen Verein, bzw. ein Projekt, oder auch mehrere, mit möglichst ähnlichen Zielen weitergegeben.

## Kosten
Die monatlichen Fixkosten für die Infrastruktur belaufen sich momentan auf:
- 18,67&#8239;€ Hauptserver
- 3,81&#8239;€ für die Domain graz.social
- 3,28&#8239;€ Mail-Hosting

## Zahlungsarten
Finanziell kann man uns wie folgt unterstützen:
- Überweisung: bitte für die Kontodaten uns persönlich via [Mastodon](https://graz.social/@wir) oder [E-Mail](/impressum) kontaktieren!
- Bar: per Brief an die Adresse im [Impressum](/impressum)
- PayPal: `wir {at} graz.social`
- Liberapay: https://liberapay.com/graz.social/

Teile uns bei deiner Spende gerne ein Zeichen mit, unter der wir deine Spende listen sollen. Ansonsten wird deine Spende lediglich mit Datum und Betrag aufgeführt und ist für dich unter Umständen nicht immer eindeitig auf dich zurück zu führen.

## Gesamtbilanz
{{< balance >}}

## Transparenz
### Liste aller Spenden
Wöchentliche Spenden via Liberapay werden als einmalige Spenden zum Zeitpunkt der Erneuerung erfasst.

{{< donations >}}

### Liste aller Ausgaben
{{< expenses >}}

## Archiv
Spenden und Ausgaben vor 2022 sind [hier](/spendenarchiv) archiviert.
