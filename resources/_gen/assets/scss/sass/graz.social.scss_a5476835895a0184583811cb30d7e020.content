.container {
  margin-left: auto;
  margin-right: auto;
  padding-left: 0.4rem;
  padding-right: 0.4rem;
  width: 100%; }
  .container.grid-xl {
    max-width: 1296px; }
  .container.grid-lg {
    max-width: 976px; }
  .container.grid-md {
    max-width: 856px; }
  .container.grid-sm {
    max-width: 616px; }
  .container.grid-xs {
    max-width: 496px; }

.show-xs,
.show-sm,
.show-md,
.show-lg,
.show-xl {
  display: none !important; }

.cols,
.columns {
  display: flex;
  flex-wrap: wrap;
  margin-left: -0.4rem;
  margin-right: -0.4rem; }
  .cols.col-gapless,
  .columns.col-gapless {
    margin-left: 0;
    margin-right: 0; }
    .cols.col-gapless > .column,
    .columns.col-gapless > .column {
      padding-left: 0;
      padding-right: 0; }
  .cols.col-oneline,
  .columns.col-oneline {
    flex-wrap: nowrap;
    overflow-x: auto; }

[class~="col-"],
.column {
  flex: 1;
  max-width: 100%;
  padding-left: 0.4rem;
  padding-right: 0.4rem; }
  [class~="col-"].col-12, [class~="col-"].col-11, [class~="col-"].col-10, [class~="col-"].col-9, [class~="col-"].col-8, [class~="col-"].col-7, [class~="col-"].col-6, [class~="col-"].col-5, [class~="col-"].col-4, [class~="col-"].col-3, [class~="col-"].col-2, [class~="col-"].col-1, [class~="col-"].col-auto,
  .column.col-12,
  .column.col-11,
  .column.col-10,
  .column.col-9,
  .column.col-8,
  .column.col-7,
  .column.col-6,
  .column.col-5,
  .column.col-4,
  .column.col-3,
  .column.col-2,
  .column.col-1,
  .column.col-auto {
    flex: none; }

.col-12 {
  width: 100%; }

.col-11 {
  width: 91.66666667%; }

.col-10 {
  width: 83.33333333%; }

.col-9 {
  width: 75%; }

.col-8 {
  width: 66.66666667%; }

.col-7 {
  width: 58.33333333%; }

.col-6 {
  width: 50%; }

.col-5 {
  width: 41.66666667%; }

.col-4 {
  width: 33.33333333%; }

.col-3 {
  width: 25%; }

.col-2 {
  width: 16.66666667%; }

.col-1 {
  width: 8.33333333%; }

.col-auto {
  flex: 0 0 auto;
  max-width: none;
  width: auto; }

.col-mx-auto {
  margin-left: auto;
  margin-right: auto; }

.col-ml-auto {
  margin-left: auto; }

.col-mr-auto {
  margin-right: auto; }

@media (max-width: 1280px) {
  .col-xl-12,
  .col-xl-11,
  .col-xl-10,
  .col-xl-9,
  .col-xl-8,
  .col-xl-7,
  .col-xl-6,
  .col-xl-5,
  .col-xl-4,
  .col-xl-3,
  .col-xl-2,
  .col-xl-1,
  .col-xl-auto {
    flex: none; }
  .col-xl-12 {
    width: 100%; }
  .col-xl-11 {
    width: 91.66666667%; }
  .col-xl-10 {
    width: 83.33333333%; }
  .col-xl-9 {
    width: 75%; }
  .col-xl-8 {
    width: 66.66666667%; }
  .col-xl-7 {
    width: 58.33333333%; }
  .col-xl-6 {
    width: 50%; }
  .col-xl-5 {
    width: 41.66666667%; }
  .col-xl-4 {
    width: 33.33333333%; }
  .col-xl-3 {
    width: 25%; }
  .col-xl-2 {
    width: 16.66666667%; }
  .col-xl-1 {
    width: 8.33333333%; }
  .col-xl-auto {
    width: auto; }
  .hide-xl {
    display: none !important; }
  .show-xl {
    display: block !important; } }

@media (max-width: 960px) {
  .col-lg-12,
  .col-lg-11,
  .col-lg-10,
  .col-lg-9,
  .col-lg-8,
  .col-lg-7,
  .col-lg-6,
  .col-lg-5,
  .col-lg-4,
  .col-lg-3,
  .col-lg-2,
  .col-lg-1,
  .col-lg-auto {
    flex: none; }
  .col-lg-12 {
    width: 100%; }
  .col-lg-11 {
    width: 91.66666667%; }
  .col-lg-10 {
    width: 83.33333333%; }
  .col-lg-9 {
    width: 75%; }
  .col-lg-8 {
    width: 66.66666667%; }
  .col-lg-7 {
    width: 58.33333333%; }
  .col-lg-6 {
    width: 50%; }
  .col-lg-5 {
    width: 41.66666667%; }
  .col-lg-4 {
    width: 33.33333333%; }
  .col-lg-3 {
    width: 25%; }
  .col-lg-2 {
    width: 16.66666667%; }
  .col-lg-1 {
    width: 8.33333333%; }
  .col-lg-auto {
    width: auto; }
  .hide-lg {
    display: none !important; }
  .show-lg {
    display: block !important; } }

@media (max-width: 840px) {
  .col-md-12,
  .col-md-11,
  .col-md-10,
  .col-md-9,
  .col-md-8,
  .col-md-7,
  .col-md-6,
  .col-md-5,
  .col-md-4,
  .col-md-3,
  .col-md-2,
  .col-md-1,
  .col-md-auto {
    flex: none; }
  .col-md-12 {
    width: 100%; }
  .col-md-11 {
    width: 91.66666667%; }
  .col-md-10 {
    width: 83.33333333%; }
  .col-md-9 {
    width: 75%; }
  .col-md-8 {
    width: 66.66666667%; }
  .col-md-7 {
    width: 58.33333333%; }
  .col-md-6 {
    width: 50%; }
  .col-md-5 {
    width: 41.66666667%; }
  .col-md-4 {
    width: 33.33333333%; }
  .col-md-3 {
    width: 25%; }
  .col-md-2 {
    width: 16.66666667%; }
  .col-md-1 {
    width: 8.33333333%; }
  .col-md-auto {
    width: auto; }
  .hide-md {
    display: none !important; }
  .show-md {
    display: block !important; } }

@media (max-width: 600px) {
  .col-sm-12,
  .col-sm-11,
  .col-sm-10,
  .col-sm-9,
  .col-sm-8,
  .col-sm-7,
  .col-sm-6,
  .col-sm-5,
  .col-sm-4,
  .col-sm-3,
  .col-sm-2,
  .col-sm-1,
  .col-sm-auto {
    flex: none; }
  .col-sm-12 {
    width: 100%; }
  .col-sm-11 {
    width: 91.66666667%; }
  .col-sm-10 {
    width: 83.33333333%; }
  .col-sm-9 {
    width: 75%; }
  .col-sm-8 {
    width: 66.66666667%; }
  .col-sm-7 {
    width: 58.33333333%; }
  .col-sm-6 {
    width: 50%; }
  .col-sm-5 {
    width: 41.66666667%; }
  .col-sm-4 {
    width: 33.33333333%; }
  .col-sm-3 {
    width: 25%; }
  .col-sm-2 {
    width: 16.66666667%; }
  .col-sm-1 {
    width: 8.33333333%; }
  .col-sm-auto {
    width: auto; }
  .hide-sm {
    display: none !important; }
  .show-sm {
    display: block !important; } }

@media (max-width: 480px) {
  .col-xs-12,
  .col-xs-11,
  .col-xs-10,
  .col-xs-9,
  .col-xs-8,
  .col-xs-7,
  .col-xs-6,
  .col-xs-5,
  .col-xs-4,
  .col-xs-3,
  .col-xs-2,
  .col-xs-1,
  .col-xs-auto {
    flex: none; }
  .col-xs-12 {
    width: 100%; }
  .col-xs-11 {
    width: 91.66666667%; }
  .col-xs-10 {
    width: 83.33333333%; }
  .col-xs-9 {
    width: 75%; }
  .col-xs-8 {
    width: 66.66666667%; }
  .col-xs-7 {
    width: 58.33333333%; }
  .col-xs-6 {
    width: 50%; }
  .col-xs-5 {
    width: 41.66666667%; }
  .col-xs-4 {
    width: 33.33333333%; }
  .col-xs-3 {
    width: 25%; }
  .col-xs-2 {
    width: 16.66666667%; }
  .col-xs-1 {
    width: 8.33333333%; }
  .col-xs-auto {
    width: auto; }
  .hide-xs {
    display: none !important; }
  .show-xs {
    display: block !important; } }

.label {
  border-radius: 0.1rem;
  line-height: 1.25;
  padding: .1rem .2rem;
  background: #eef0f3;
  color: #455060;
  display: inline-block; }
  .label.label-rounded {
    border-radius: 5rem;
    padding-left: .4rem;
    padding-right: .4rem; }
  .label.label-primary {
    background: #5755d9;
    color: #fff; }
  .label.label-secondary {
    background: #f1f1fc;
    color: #5755d9; }
  .label.label-success {
    background: #1f7f2c;
    color: #fff; }
  .label.label-warning {
    background: #a37000;
    color: #fff; }
  .label.label-error {
    background: #c32a04;
    color: #fff; }

.modal {
  align-items: center;
  bottom: 0;
  display: none;
  justify-content: center;
  left: 0;
  opacity: 0;
  overflow: hidden;
  padding: 0.4rem;
  position: fixed;
  right: 0;
  top: 0; }
  .modal:target, .modal.active {
    display: flex;
    opacity: 1;
    z-index: 400; }
    .modal:target .modal-overlay, .modal.active .modal-overlay {
      background: var(--theme);
      opacity: 0.75;
      bottom: 0;
      cursor: default;
      display: block;
      left: 0;
      position: absolute;
      right: 0;
      top: 0; }
    .modal:target .modal-container, .modal.active .modal-container {
      animation: slide-down .2s ease 1;
      z-index: 1; }
  .modal.modal-sm .modal-container {
    max-width: 320px;
    padding: 0 0.4rem; }
  .modal.modal-lg .modal-overlay {
    background: #fff; }
  .modal.modal-lg .modal-container {
    box-shadow: none;
    max-width: 960px; }

.modal-container {
  box-shadow: 0 0.2rem 0.5rem rgba(48, 55, 66, 0.3);
  background: var(--entry);
  border-radius: 0.1rem;
  display: flex;
  flex-direction: column;
  max-height: 75vh;
  max-width: 640px;
  padding: 0 0.8rem;
  width: 100%; }
  .modal-container.modal-fullheight {
    max-height: 100vh; }
  .modal-container .modal-header {
    color: var(--primary);
    padding: 0.8rem; }
  .modal-container .modal-body {
    overflow-y: auto;
    padding: 0.8rem;
    position: relative; }
    .modal-container .modal-body ul {
      margin-left: 2rem;
      padding-bottom: 1rem; }
    .modal-container .modal-body a {
      text-decoration: underline; }
  .modal-container .modal-footer {
    padding: 0.8rem;
    text-align: right; }

.btn {
  appearance: none;
  background: #fff;
  border: 0.05rem solid #5755d9;
  border-radius: 0.1rem;
  color: #5755d9;
  cursor: pointer;
  display: inline-block;
  font-size: 0.8rem;
  height: 1.8rem;
  line-height: 1.2rem;
  outline: none;
  padding: 0.25rem 0.4rem;
  text-align: center;
  text-decoration: none;
  transition: background .2s, border .2s, box-shadow .2s, color .2s;
  user-select: none;
  vertical-align: middle;
  white-space: nowrap; }
  .btn:focus {
    box-shadow: 0 0 0 0.1rem rgba(87, 85, 217, 0.2); }
  .btn:focus, .btn:hover {
    background: #f1f1fc;
    border-color: #4b48d6;
    text-decoration: none; }
  .btn:active, .btn.active {
    background: #4b48d6;
    border-color: #3634d2;
    color: #fff;
    text-decoration: none; }
    .btn:active.loading::after, .btn.active.loading::after {
      border-bottom-color: #fff;
      border-left-color: #fff; }
  .btn[disabled], .btn:disabled, .btn.disabled {
    cursor: default;
    opacity: .5;
    pointer-events: none; }
  .btn.btn-primary {
    background: #5755d9;
    border-color: #4b48d6;
    color: #fff; }
    .btn.btn-primary:focus, .btn.btn-primary:hover {
      background: #4240d4;
      border-color: #3634d2;
      color: #fff; }
    .btn.btn-primary:active, .btn.btn-primary.active {
      background: #3a38d2;
      border-color: #302ecd;
      color: #fff; }
    .btn.btn-primary.loading::after {
      border-bottom-color: #fff;
      border-left-color: #fff; }
  .btn.btn-success {
    background: #1f7f2c;
    border-color: #1c7328;
    color: #fff; }
    .btn.btn-success:focus {
      box-shadow: 0 0 0 0.1rem rgba(31, 127, 44, 0.2); }
    .btn.btn-success:focus, .btn.btn-success:hover {
      background: #1d7729;
      border-color: #1a6b25;
      color: #fff; }
    .btn.btn-success:active, .btn.btn-success.active {
      background: #186222;
      border-color: #15561e;
      color: #fff; }
    .btn.btn-success.loading::after {
      border-bottom-color: #fff;
      border-left-color: #fff; }
  .btn.btn-error {
    background: #c32a04;
    border-color: #b42704;
    color: #fff; }
    .btn.btn-error:focus {
      box-shadow: 0 0 0 0.1rem rgba(195, 42, 4, 0.2); }
    .btn.btn-error:focus, .btn.btn-error:hover {
      background: #b92804;
      border-color: #aa2503;
      color: #fff; }
    .btn.btn-error:active, .btn.btn-error.active {
      background: #a02203;
      border-color: #911f03;
      color: #fff; }
    .btn.btn-error.loading::after {
      border-bottom-color: #fff;
      border-left-color: #fff; }
  .btn.btn-link {
    background: transparent;
    border-color: transparent;
    color: #5755d9; }
    .btn.btn-link:focus, .btn.btn-link:hover, .btn.btn-link:active, .btn.btn-link.active {
      color: #302ecd; }
  .btn.btn-sm {
    font-size: 0.7rem;
    height: 1.4rem;
    padding: 0.05rem 0.3rem; }
  .btn.btn-lg {
    font-size: 0.9rem;
    height: 2rem;
    padding: 0.35rem 0.6rem; }
  .btn.btn-block {
    display: block;
    width: 100%; }
  .btn.btn-action {
    width: 1.8rem;
    padding-left: 0;
    padding-right: 0; }
    .btn.btn-action.btn-sm {
      width: 1.4rem; }
    .btn.btn-action.btn-lg {
      width: 2rem; }
  .btn.btn-clear {
    background: transparent;
    border: 0;
    color: currentColor;
    height: 1rem;
    line-height: 0.8rem;
    margin-left: 0.2rem;
    margin-right: -2px;
    opacity: 1;
    padding: 0.1rem;
    text-decoration: none;
    width: 1rem; }
    .btn.btn-clear:focus, .btn.btn-clear:hover {
      background: rgba(247, 248, 249, 0.5);
      opacity: .95; }
    .btn.btn-clear::before {
      content: "\2715"; }

.btn-group {
  display: inline-flex;
  flex-wrap: wrap; }
  .btn-group .btn {
    flex: 1 0 auto; }
    .btn-group .btn:first-child:not(:last-child) {
      border-bottom-right-radius: 0;
      border-top-right-radius: 0; }
    .btn-group .btn:not(:first-child):not(:last-child) {
      border-radius: 0;
      margin-left: -0.05rem; }
    .btn-group .btn:last-child:not(:first-child) {
      border-bottom-left-radius: 0;
      border-top-left-radius: 0;
      margin-left: -0.05rem; }
    .btn-group .btn:focus, .btn-group .btn:hover, .btn-group .btn:active, .btn-group .btn.active {
      z-index: 1; }
  .btn-group.btn-group-block {
    display: flex; }
    .btn-group.btn-group-block .btn {
      flex: 1 0 0; }

.header-image-aside {
  padding-top: 3rem;
  position: fixed;
  max-width: 426px !important; }

body.dark #label-light {
  vertical-align: middle;
  display: none; }

body:not(.dark) #label-dark {
  display: none; }

#inline-logo {
  margin-top: -20px; }

.float-right {
  float: right; }

.framadate {
  font-family: DejaVu Sans,Verdana,Geneva,sans-serif; }

.framadate-violet {
  color: #725794; }

.framadate-vert {
  color: #6c7e31; }

.post-title > svg {
  height: 70px; }

.breadcrumbs {
  margin-bottom: 1rem; }

.service-link {
  font-size: 17px;
  color: var(--secondary); }

.post-header > .post-tags {
  margin-top: 12px; }
  .post-header > .post-tags li.replacements {
    padding-inline-start: 10px;
    padding-inline-end: 10px;
    color: var(--secondary);
    font-size: 14px;
    line-height: 34px;
    background: var(--code-bg);
    border-radius: var(--radius);
    border: 1px solid var(--border); }
  .post-header > .post-tags li:first-child {
    margin-left: 6px; }

.font-larger {
  font-size: 20px !important;
  line-height: 40px !important; }

.entry-cover {
  position: absolute;
  right: 10px;
  top: 10px;
  top: 50%;
  transform: translateY(-50%); }
  .entry-cover > img {
    max-height: 70px;
    width: auto;
    margin-right: auto; }

svg {
  max-width: 100%; }

.width-buffer-for-icon {
  width: calc(100% - 75px); }

.main {
  padding: var(--gap) 8px; }
