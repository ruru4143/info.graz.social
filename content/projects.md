---
title: Event Federation Plugin for WordPress
summary: Add ActivityPub to events created with most common WordPress event plugins
date: 2023-04-04
ShowBreadCrumbs: false
---

## More information will follow

Freedom in announcing events. The WordPress Event Federation plugin allows events created in WordPress with the most popular event plugins to be seamlessly published to Fediverse via ActivityPub. The core problem is that events need to be discoverable, listable and subscribable by potential visitors. Since organisers' personal websites do not meet this requirement, most of them publish their events on multiple (commercial) platforms, which results in people searching for events being tied to these platforms. Currently, many to most event organisers use WordPress to run their own website. With this plugin, they can make their events even more visible without changing their workflow. At the same time, they gain data sovereignty and independence from traditional search engines and platforms that give less control over how content can be filtered. The goal is to realise typical use cases, such as server-to-server federation with Mobilizon instances, or another example: to allow Fediverse users, such as those of Mastodon, to follow events directly from the organisers.

Funded by NLnet https://nlnet.nl/project/WordPress-EventFederation/
