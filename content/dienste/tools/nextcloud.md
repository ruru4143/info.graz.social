---
title: Nextcloud
type: tool
link: "https://cloud.graz.social"
weight: 10
tags: ["Tool", "Cloud", "Umfragen"]
replaces: ["Google-Drive", "Dropbox"]
label:
    name: nextcloud
    replacesTitle: true
    height: 80
---

Mehrmals wurde danach gefragt: Ab sofort steht für Privatpersonen und (gemeinnützige) Organisationen und Vereine **auf Anfrage** hin eine Nextcloud zur Verfügung.
Bitte einfach über [Mastodon](https://graz.social/@linos) oder per E-Mail [-> Impressum](https://info.graz.social/impressum) melden.

## Features
- Gemeinsames Editieren von Dokumenten.
- Gemeinsame Dateiablage, wobei das Verwalten von Berechtigungen mittels Accounts oder geteilten Links möglich ist.
- Erstellen eigene Kreise (Communities)!

## Registrierung
Da eine Cloud in der Regel etwas sehr persönliches ist und der Nutzen überwiegend privat ist und diese schnell sehr viel Speicherplatz benötigt ist eine Registrierung nur manuell möglich. Wenn du gerne einen Account hättest, zögere nicht und kontaktiere uns einfach.