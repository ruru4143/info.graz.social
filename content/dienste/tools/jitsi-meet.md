---
title: Jitsi-Meet
type: tool
featured-image: jitsi-meet.jpg
link: "https://meet.graz.social"
weight: 7
tags: ["Tool", "Videocall", "Meeting"]
replaces: ["Zoom", "Skype"]
label:
    name: jitsi
    replacesTitle: true
    height: 40
---

# Eingestellt
Der Betrieb unserer Jitsi Servers wurde eingestellt, weil wir momentan nicht die Ressourcen dafür haben.
Bitte auf Alternativen wie https://www.senfcall.de/ ausweichen.

## Audio- und Videokonferenz für kleinere Gruppen:
- Voller Funktionsumfang auch direkt im Browser
- Anonyme Nutzung (Registrierung ist nicht erforderlich)
- Ende-zu-Ende Verschlüsselt bei maximal zwei TeilnehmerInnen
- Teilen von Audio und Bilschirm

## Screenshot
![Screenshot, der eine Videokonferenz mit Jitsi-Meet zeigt](../images/jitsi-meet.jpg)

## Apps
- Mobile Apps verfügbar, Android ([F-Droid ohne Google-Tracker](https://f-droid.org/de/packages/org.jitsi.meet/), [Play Store](https://play.google.com/store/apps/details?id=org.jitsi.meet&hl=en)) und [iOS](https://itunes.apple.com/us/app/jitsi-meet/id1165103905)
