---
title: Spendenarchiv
ShowBreadCrumbs: false
---

# Archiv

#### Bilanz zum 1.1.2022 00:00 Uhr
|                |          |
|----------------|----------|
| Summe Ausgaben | 503,80 € |
| Summe Spenden  | 704,54 € |
|---------------------------|
| Kontostand     | <span class="label label-success">+200,74€</span> |

#### Monatliche Zusammenfassung

##### Dezember 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  8,40 € |

##### November 2021
|                |         |
|----------------|---------|
| Einmalig       | 23,00 € |
| Daueraufträge  |  8,40 € |

##### Oktober 2021
|                |         |
|----------------|---------|
| Einmalig       |225,00 € |
| Daueraufträge  |  8,40 € |

##### September 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  8,40 € |

##### August 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  8,40 € |


##### Juli 2021
|                |         |
|----------------|---------|
| Einmalig       | 40,00 € |
| Daueraufträge  |  8,40 € |


##### Juni 2021
|                |         |
|----------------|---------|
| Einmalig       | 27,21 € |
| Daueraufträge  |  8,40 € |


##### Mai 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  5,90 € |


##### April 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  5,90 € |


##### März 2021
|                |         |
|----------------|---------|
| Einmalig       | 70,00 € |
| Daueraufträge  |  5,90 € |


##### Februar 2021
|                |         |
|----------------|---------|
| Einmalig       | 16,19 € |
| Daueraufträge  |  5,90 € |


##### Jänner 2021
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 5,90 €  |



##### Dezember 2020
|                |         |
|----------------|---------|
| Einmalig       | 50,00 € |
| Daueraufträge  | 3,90 €  |


##### November 2020
|                |         |
|----------------|---------|
| Einmalig       | 86,74 € |
| Daueraufträge  | 3,90 €  |

##### Oktober 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### September 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |


##### August 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### Juli 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### Juni 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### Juni 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### Mai 2020
|                |         |
|----------------|---------|
| Einmalig       | 20,00 € |
| Daueraufträge  | 3,90 €  |

##### April 2020
|                |         |
|----------------|---------|
| Einmalig       | 38,00 € |
| Daueraufträge  | 0,00 €  |
