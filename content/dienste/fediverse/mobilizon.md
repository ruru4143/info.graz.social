---
title: "Mobilizon"
type: fediverse
link: "https://events.graz.social"
weight: 5
replaces: ["Facebook-Events", "Facebook-Gruppen"]
tags: ["Fediverse", "Soziales-Netzwerk", "Events"]
label:
  name: mobilizon
  replacesTitle: true
  height: 30
---

Die neue föderierte Alternative zu Facebook-Events und Facebook-Gruppen!

## Screenshot
![Screenshot der Eventansicht von Mobilizon](../images/screenshots/mobilizon.jpg)

## Beschreibung:
- Eine Mobilizon-Instanz, kann anderen Mobilizon-Instanzen folgen: zum Beispiel folgen wir bereits https://mobi.climatejustice.global (wurde erst Mitte Mai 2021 ins Leben gerufen). Das hat zur Folge, dass alle Veranstaltungen, die dort erstellt werden, bei uns ebenfalls angezeigt werden.
- Man kann einzelnen Gruppen, beziehungsweise Nutzer*innen folgen: z.B. über Mastodon, über RSS oder direkt mit der Kalender-App deines Smartphones. So hast du die Möglichkeit neue Veranstaltungen deiner Lieblingsveranstalter mitzubekommen, ohne aktiv danach suchen zu müssen. Wenn du Beispielsweise wissen willst, wann es die nächste Plfanzentauschaktion im Attemsgarten gibt, folge dem [SEED Kollektiv](https://events.graz.social/@seedkollektiv).
- Innerhalb der Gruppen:
  - Interne Diskussionsforen
  - Direkte Integration von [Etherpad](/dienste/tools/etherpad/) und [Jitsi-Meet](/dienste/tools/jitsi-meet/)
  - Veröffentlichen von Beiträgen und Veranstaltungen auf der öffentlichen Seite

Mobilizon benutzt das selbe Protokoll wie Mastodon, Pixelfed oder PeerTube: [ActivityPub](https://de.wikipedia.org/wiki/ActivityPub)

## Links
Weite Informationen findet man unter [https://joinmobilizon.org/](https://joinmobilizon.org/)
