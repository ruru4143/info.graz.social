---
title: Lemmy
link: "https://lemmy.graz.social"
type: fediverse
weight: 4
tags: ["Fediverse", "Linkaggregator", "Journalismus"]
replaces: ["Reddit"]
label:
    name: Lemmy
    replacesTitle: falce
    height: 33
---

Die dezentrale Alternative für Reddit: diskutieren über Inhalte auf externen Links.

Zitat von der Seite der Entwickler*innen [https://join-lemmy.org/](https://join-lemmy.org/):
> Lemmy ist ähnlich zu Seiten wie Reddit, Lobste.rs, oder Hacker News: du abonnierst Communities an denen du interessiert bist, postest Links und Diskussionen, und votest und kommentierst sie dann. Lemmy ist nicht einfach nur eine Reddit Alternative; es ist ein Netzwerk von verbundenen Communities, die von verschiedenen Leuten und Organisation geführt werden, die alle zusammengeschlossen eine einzelne, personalisierte Startseite erschaffen, bestehend aus deinen Lieblingsnews, Artikeln und Memes.

## Registrierung
Aufgrund mangelnder Kapazitäten in der Moderation wird eine jede Registrierung manuell bestätigt, dass kann auch mal einen Tag dauern.