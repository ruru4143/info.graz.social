---
title: "Fediverse"
summary: "Föderierte Soziale Netzwerke"
type: fediverse
section: fediverse
cover:
    image: "/dienste/fediverse/images/fediverse.png"
    relative: false
    hidden: false
    alt: "Fediverse"
---

Zum Fediverse gehören verschiedenste Plattformen, wir betreiben die verbreitetsten davon.