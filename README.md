# Website unseres Projektes: https://info.graz.social

## Vorschläge zum Content:

Fühle Dich eingeladen, einen [Issue](https://codeberg.org/graz.social/info.graz.social/issues) bzw. Pull-Requests zu erstellen, wenn du irgendeine Art von Verbesserungsideen oder konkrete Vorschläge hast, oder schreibe uns auf [Mastodon](https://info.graz.social/@wir) oder per [Mail](https://info.graz.social/impressum).
